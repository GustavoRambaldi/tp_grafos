package java_tp_grafos;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 *
 * @author Rambaldi Gonçalves Vidal Magalhães
 */
public class FileIO {

    private static final String VIRGULA = ",";

    public static void readVertex(String path) throws Exception {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {
            String linha = reader.readLine();
            while ((linha = reader.readLine()) != null) {
                String[] dados = linha.split(VIRGULA);
                Main.vertices.put(dados[0], new Vertex(dados[0], dados[1], dados[2], dados[3]));
            }
        }
    }

    public static void readEdge(String path) throws Exception {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {
            String linha = reader.readLine();
            while ((linha = reader.readLine()) != null) {
                String[] dados = linha.split(VIRGULA);
                if ((!"crosslayer".equals(dados[3]))) {
                    Main.vertices.get(dados[0]).adj.add(new Edge(dados[2], dados[0], dados[1], dados[3], dados[4]));
                    if(dados[3].equals("TwoWay")){
                        Main.vertices.get(dados[1]).adj.add(new Edge(dados[2], dados[1], dados[0]," ", dados[4]));
                    }
                }
            }
        }
    }
}
