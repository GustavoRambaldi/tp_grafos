package java_tp_grafos;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rambaldi
 */
public class Main {
    public static Map<String,Vertex> vertices = new HashMap<String,Vertex>();
    public static Map<String,Edge> arestas = new HashMap<String,Edge>();
    
    /*public static void main(String[] args) throws Exception {
        FileIO.readVertex("vertex.csv");
        FileIO.readEdge("Edge.csv");
        Graph.printListAdj(vertices);
    }*/ 
    
    public static void main(String[] args) throws Exception {
        vertices.put("01", new Vertex("01", "2", "2", "road"));
        vertices.put("02", new Vertex("02", "1", "7", "road"));
        vertices.put("03", new Vertex("03", "2", "4", "road"));
        vertices.put("04", new Vertex("04", "5", "10", "road"));
        vertices.put("05", new Vertex("05", "6", "6", "road"));
        vertices.put("06", new Vertex("06", "9", "9", "road"));
        vertices.put("07", new Vertex("07", "2", "5", "road"));
        vertices.put("08", new Vertex("08", "8", "4", "road"));
        vertices.put("09", new Vertex("09", "5", "3", "road"));
        vertices.put("10", new Vertex("10", "6", "1", "road"));
        
        FileIO.readEdge("adge.txt");
        Dijkstra.djikstra(vertices.get("01"), vertices.get("02"), vertices);
        
        //Graph.printListAdj(vertices);
    }
}
