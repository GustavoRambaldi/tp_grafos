package java_tp_grafos;

/**
 *
 * @author Rambaldi
 */
public class Edge {

    private static final double R = 6371000; //metros 

    private final String source;
    private final String target;
    private final String id;
    private final String direction;
    private final String layer;
    private double peso;

    public Edge(String id, String source, String target, String direction, String layer) {
        this.source = source;
        this.target = target;
        this.id = id;
        this.direction = direction;
        this.layer = layer;

        double[] ds = Main.vertices.get(source).getPos();
        double[] dt = Main.vertices.get(target).getPos();

        this.peso = distance(ds[0], ds[1], dt[0], dt[1]);
    }

    public static double distance(double startLat, double startLong, double endLat, double endLong) {

        double dLat = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat = Math.toRadians(endLat);

        double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c;
    }

    public static double haversin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }

    public String getDirection() {
        return direction;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public String getId() {
        return id;
    }

    public String getLayer() {
        return layer;
    }

    public double getPeso() {
        return peso;
    }
    
    
}
