package java_tp_grafos;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Rambaldi
 */
public class Vertex implements Comparable{

    private final String id;
    private final double lat;
    private final double lon;
    private final String layer;
    private double distance;
    public List<Edge> adj = new ArrayList<>();

    private static int n = 0;

    public Vertex(String id, String lat, String lon, String layer) {
        this.id = id;
        this.lat = Double.parseDouble(lat);
        this.lon = Double.parseDouble(lon);
        this.layer = layer;
        n++;
    }

    public double[] getPos() {
        double pos[] = {this.lat, this.lon};
        return pos;
    }

    public static int getN() {
        return n;
    }

    public String getId() {
        return id;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int compareTo(Vertex a, Vertex b) {
        double r = a.distance - b.distance;
        if(r<0){
            return 1;
        }
        if(r>0){
            return -1;
        }
        return 0;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
