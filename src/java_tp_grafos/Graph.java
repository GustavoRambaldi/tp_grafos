package java_tp_grafos;

import java.util.Map;

/**
 *
 * @author Rambaldi
 */
public class Graph {
    public static void printListAdj(Map<String, Vertex> vertices){
        System.out.println("Lista de adjacencia :");
        for (Vertex v : vertices.values()){
            System.out.print(v.getId() + ": ");
            for (Edge t : v.adj){
                System.out.print(t.getTarget() + " ");
            }
            System.out.println();
        }
    }
}
