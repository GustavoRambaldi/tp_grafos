package java_tp_grafos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rambaldi
 */
public class Dijkstra {

    private static List<Vertex> unsettled = new ArrayList<>();
    private static List<Vertex> settled = new ArrayList<>();
    private static Vertex evaluation;

    public static void djikstra(Vertex init, Vertex end, Map<String, Vertex> vertices) {
        for (Vertex v : vertices.values()) {
            v.setDistance(999999);
        }
        vertices.get(init.getId()).setDistance(0);
        unsettled.add(init);
        evaluation = init;
        settled.add(evaluation);

        while (!unsettled.isEmpty()) {
            unsettled.remove(evaluation);
            System.out.println(evaluation.getId());
            for (Edge e : evaluation.adj) {
                if (!unsettled.contains(vertices.get(e.getTarget()))) {
                    unsettled.add(vertices.get(e.getTarget()));
                    vertices.get(e.getTarget()).setDistance(e.getPeso());
                }
            }
        }
    }
}
